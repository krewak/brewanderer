let sourceCountries = [
	{ "symbol": "pl", "name": "Polska" },
	{ "symbol": "at", "name": "Austria" },
	{ "symbol": "de", "name": "Niemcy" },
	{ "symbol": "cz", "name": "Czechy" },
	{ "symbol": "al", "name": "Albania" },
	{ "symbol": "ba", "name": "Bośnia i Hercegowina" },
	{ "symbol": "it", "name": "Włochy" },
	{ "symbol": "si", "name": "Słowenia" },
	{ "symbol": "gr", "name": "Grecja" },
	{ "symbol": "nl", "name": "Holandia" },
	{ "symbol": "ee", "name": "Estonia" },
	{ "symbol": "ua", "name": "Ukraina" },
];
